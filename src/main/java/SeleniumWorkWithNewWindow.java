import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class SeleniumWorkWithNewWindow {
  public static void main(String[] args) throws InterruptedException {
    WebDriver driver = new ChromeDriver();
    driver.get("https://the-internet.herokuapp.com/");
    driver.manage().window().maximize();
    List<WebElement> listOfLinks = driver.findElements(By.cssSelector("div>ul>li>a"));
    listOfLinks.get(32).click();
    Thread.sleep(3000);
    WebElement clickHereBtn = driver.findElement(By.cssSelector("a[href=\"/windows/new\"]"));
    clickHereBtn.click();
    String mainWindow = driver.getWindowHandle();//12345
    Set<String> windowsId = driver.getWindowHandles();//id1 =  12345, id2 = 123456
    Iterator<String> iterator = windowsId.iterator();
    while (iterator.hasNext()) {
      String childWindow = iterator.next();//12345,123456
      //12345                           //123456
      if (!mainWindow.equalsIgnoreCase(childWindow)) {

        driver.switchTo().window(childWindow);
        WebElement h3 = driver.findElement(By.xpath("//h3"));
        System.out.println(h3.getText());
        driver.close();
      }
    }

    driver.quit();
  }
}
