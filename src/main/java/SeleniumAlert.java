import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;



public class SeleniumAlert {
  public static void main(String[] args) throws InterruptedException {
    WebDriver driver = new ChromeDriver();
    driver.get("https://testpages.herokuapp.com/styled/index.html");
    driver.manage().window().maximize();
    WebElement alert = driver.findElement(By.cssSelector("#alerttest"));
    alert.click();
    WebElement showAlertBox = driver.findElement(By.cssSelector("#alertexamples"));
    WebElement showConfirmBox = driver.findElement(By.cssSelector("#confirmexample"));
    WebElement showPromptBox = driver.findElement(By.cssSelector("#promptexample"));
//    showAlertBox.click();
//    showConfirmBox.click();
    showPromptBox.click();

//   driver.switchTo().alert().accept();
//    driver.switchTo().alert().dismiss();
    driver.switchTo().alert().sendKeys("Hello");
    driver.switchTo().alert().accept();

    driver.quit();
  }
}
