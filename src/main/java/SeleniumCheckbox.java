import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.List;

public class SeleniumCheckbox {
  public static void main(String[] args) throws InterruptedException {
    WebDriver driver = new ChromeDriver();
    driver.get("https://the-internet.herokuapp.com/");
    driver.manage().window().maximize();
    List<WebElement> listOfLinks = driver.findElements(By.cssSelector("div>ul>li>a"));
    listOfLinks.get(5).click();
    List<WebElement> listOfCheckbox = driver.findElements(By.cssSelector("#checkboxes>input"));
    listOfCheckbox.get(0).click();
    listOfCheckbox.get(1).click();
    System.out.println(listOfCheckbox.get(0).getAttribute("checked"));
    System.out.println(listOfCheckbox.get(0).isSelected());
    System.out.println(listOfCheckbox.get(1).isSelected());
    Thread.sleep(3000);
    driver.quit();
  }
}
