import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class DragAndDrop {
  public static void main(String[] args) throws InterruptedException {
    WebDriver driver = new ChromeDriver();
    Actions action = new Actions(driver);
    driver.get("https://www.globalsqa.com/demo-site/draganddrop/");
    driver.manage().window().maximize();
    Thread.sleep(3000);
    WebElement frame = driver.findElement(By.xpath("//iframe[@class=\"demo-frame lazyloaded\"]"));
    driver.switchTo().frame(frame);
    WebElement img1 = driver.findElement(By.cssSelector(".ui-widget>ul>li:first-of-type"));
    WebElement trash = driver.findElement(By.cssSelector(".ui-widget-content.ui-state-default"));
    action.dragAndDrop(img1, trash).build().perform();
    Thread.sleep(3000);
    driver.quit();
  }
}
