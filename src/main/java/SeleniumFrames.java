import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;


public class SeleniumFrames {
  public static void main(String[] args) throws InterruptedException {
    WebDriver driver = new ChromeDriver();
    driver.get("https://www.globalsqa.com/demo-site/frames-and-windows/#iFrame");
    driver.manage().window().maximize();
    WebElement outFrame = driver.findElement(By.cssSelector("iframe[name=\"globalSqa\"]"));
    driver.switchTo().frame(outFrame);
    WebElement element1 = driver.findElement(By.cssSelector("a[href=\"https://www.globalsqa.com/training/selenium-online-training/\"]"));
    element1.click();
    driver.switchTo().defaultContent();
    driver.quit();
  }
}
