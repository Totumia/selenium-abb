import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.List;

public class CssSelectors {
  public static void main(String[] args) {
    /*
    css locators:
    1)id --> tag#id,#id
    2)class -->  tag.class,.classname,.class1.class2...
    3)attribute --> tag[attribute=value] can add all attributes
    4)tag.class[attribute=value]
    5)parent>childtag
    6)parent>child:first-of-type
    7)parent>child:last-of-type
    8)parent>child:nth-of-type(index)
   */

    WebDriver driver = new ChromeDriver();
    driver.get("https://www.saucedemo.com/");
    driver.manage().window().maximize();
    WebElement username = driver.findElement(By.cssSelector("input[placeholder=\"Username\"] "));//attribute
    username.sendKeys("standard_user");
    WebElement password = driver.findElement(By.cssSelector("input#password"));//id
    password.sendKeys("secret_sauce");
    WebElement loginBtn = driver.findElement(By.cssSelector("input.submit-button"));//class
    loginBtn.click();
    List<WebElement> listOfProducts = driver.findElements(By.cssSelector(".inventory_item_name"));
    for (WebElement product:listOfProducts){
      System.out.println(product.getText());
    }
    System.out.println(listOfProducts.size());
    driver.quit();
  }
}
