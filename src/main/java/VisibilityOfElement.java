import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class VisibilityOfElement {
  public static void main(String[] args) {
    WebDriver driver = new ChromeDriver();
    WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
    driver.get("https://the-internet.herokuapp.com/dynamic_loading/1");
    driver.manage().window().maximize();
    WebElement startBtn = driver.findElement(By.cssSelector("div#start>button"));
    startBtn.click();
    WebElement helloWorld = driver.findElement(By.cssSelector("div#finish>h4"));
    wait.until(ExpectedConditions.visibilityOf(helloWorld));
    System.out.println(helloWorld.isDisplayed());
    driver.quit();
  }
}
