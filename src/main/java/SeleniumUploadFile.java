import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.List;

public class SeleniumUploadFile {
  public static void main(String[] args) throws InterruptedException {
    WebDriver driver = new ChromeDriver();
    driver.get("https://the-internet.herokuapp.com/");
    driver.manage().window().maximize();
    List<WebElement> listOfLinks = driver.findElements(By.cssSelector("div>ul>li>a"));
    listOfLinks.get(17).click();
    Thread.sleep(3000);
    WebElement fileUploadInput = driver.findElement(By.cssSelector("#file-upload"));
    fileUploadInput.sendKeys("C:\\Users\\ASUS\\IdeaProjects\\selenium-locators\\src\\main\\resources\\selenium.txt");
    Thread.sleep(3000);
    driver.quit();
  }
}
