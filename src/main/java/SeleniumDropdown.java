import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

public class SeleniumDropdown {
  public static void main(String[] args) throws InterruptedException {
    WebDriver driver = new ChromeDriver();
    driver.get("https://the-internet.herokuapp.com/");
    driver.manage().window().maximize();
    List<WebElement> listOfLinks = driver.findElements(By.cssSelector("div>ul>li>a"));
    listOfLinks.get(10).click();
    WebElement options = driver.findElement(By.cssSelector("select#dropdown"));
    Select option = new Select(options);

//    option.selectByVisibleText("Option 1");
    //option.selectByValue("2");
    option.selectByIndex(1);
    System.out.println(option.getFirstSelectedOption().getText());
  System.out.println(option.isMultiple());

    Thread.sleep(3000);
    driver.quit();
  }
}

