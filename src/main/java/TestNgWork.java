import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.Duration;

public class TestNgWork {
  WebDriver driver = new ChromeDriver();

  @BeforeMethod
  public void setUp() {
    driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
    driver.get("https://opensource-demo.orangehrmlive.com/");
    driver.manage().window().maximize();
  }

  @Test
  public void loginToOrange() {
    WebElement username = driver.findElement(By.cssSelector("input[name=\"username\"]"));
    username.sendKeys("Admin");
    WebElement password = driver.findElement(By.cssSelector("input[name=\"password\"]"));
    password.sendKeys("admin123");
    WebElement loginBtn = driver.findElement(By.cssSelector(".orangehrm-login-button"));
    loginBtn.click();
    WebElement header = driver.findElement(By.cssSelector(".oxd-topbar-header-breadcrumb>h6"));
    Assert.assertEquals(header.getText(), "Dashboard", "Header not correct!!!");
  }

  @Test
  public void addEmergencyContact() {
    WebDriverWait wait = new WebDriverWait(driver,Duration.ofSeconds(10));
    WebElement username = driver.findElement(By.cssSelector("input[name=\"username\"]"));
    username.sendKeys("Admin");
    WebElement password = driver.findElement(By.cssSelector("input[name=\"password\"]"));
    password.sendKeys("admin123");
    WebElement loginBtn = driver.findElement(By.cssSelector(".orangehrm-login-button"));
    loginBtn.click();
    WebElement myInfo = driver.findElement(By.cssSelector("a[href=\"/web/index.php/pim/viewMyDetails\"]"));
    myInfo.click();
    WebElement emergencyContact = driver.findElement(By.cssSelector("a[href=\"/web/index.php/pim/viewEmergencyContacts/empNumber/7\"]"));
    emergencyContact.click();
    WebElement addBtn = driver.findElement(By.cssSelector(".orangehrm-edit-employee-content>div:first-of-type>div>button"));
    addBtn.click();
    WebElement name = driver.findElement(By.cssSelector(".oxd-form>div:nth-of-type(1)>div>div:nth-of-type(1)>div>div>input"));
    name.sendKeys("Tofig");
    WebElement toggleCheck = driver.findElement(By.cssSelector("type=\"checkbox\""));
    Assert.assertTrue(toggleCheck.isSelected());
  }

  @AfterMethod
  public void tearDown() {
    driver.quit();
  }
}
