import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import java.util.List;

public class SeleniumActions {
  public static void main(String[] args) throws InterruptedException {
    WebDriver driver = new ChromeDriver();
    Actions action = new Actions(driver);
    driver.get("https://the-internet.herokuapp.com/");
    driver.manage().window().maximize();
    List<WebElement> listOfLinks = driver.findElements(By.cssSelector("div>ul>li>a"));
    listOfLinks.get(6).click();
    Thread.sleep(3000);
    WebElement element = driver.findElement(By.cssSelector("#hot-spot"));
    action.contextClick(element).build().perform();
    Thread.sleep(3000);
    driver.switchTo().alert().accept();
    driver.quit();
  }
}
