import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class PresenceOfElement {
  public static void main(String[] args) {
    WebDriver driver = new ChromeDriver();
    WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
    driver.get("https://the-internet.herokuapp.com/dynamic_controls");
    driver.manage().window().maximize();
    WebElement removeBtn = driver.findElement(By.cssSelector("button[onclick=\"swapCheckbox()\"]"));
    removeBtn.click();
   wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("p#message")));
    WebElement text = driver.findElement(By.cssSelector("p#message"));
    System.out.println(text.getText());
    driver.quit();
  }
}
